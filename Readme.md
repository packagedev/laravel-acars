# Aircraft Communications Addressing and Reporting System (ACARS)
Messages system for Laravel

This package will allow you to set messages and dispaly them to the users on the next page load.


## Usage
First, install the package through Composer. Add the following to your `composer.json` file.
```js
"require": {
	"devosburchart/acars": "dev-master"
}
```

Second, run `composer install` or `composer update` to install the package.

Finally, add the service provider and alias to `app/config/app.php`, within the `providers` and `aliases` array respectively. 

```php
'providers' => array(
	// ...

	'DeVosBurchart\Acars\FmcServiceProvider'
)
```

```php
'aliases' => array(
	// ...
	
	'Messages' => 'DeVosBurchart\Acars\Acars',
)
```

## Assets
This package contains a css file to be shown properly.
To publish run `artisan asset:publish devosburchart/acars`
Asset can be included in your template using `<link rel="stylesheet" type="text/css" href="{{ asset('packages/devosburchart/acars/messages.css') }}" />`

## Theming
By default, the messages will have to be rendered in a wrapper with id 'console'.

## Translations
By default, any string passed through the messages function is passed through the translation function. If your message doesn't exist in the languages files, the inputted text will be shown.
If your input is a message like "Hello World", you can be fairly certain that it's not a valid string for translation as they follow the `file.key` format.

## Options
When setting single messages, several options can be added. This has to be an array passed in the second parameter.

Option | Type | Description
------ | ---- | -------
priority | integer | The order in which your messages are shown, a lower number will be shown higher while a higher number will be shown lower. Default is 0.
temp | boolean | Temporary messages are shown before redirect, they're not stored in the session and will be shown in your view as soon as the page finishes processing. This can be useful for messages you want to be shown on the same page. Default is false.
replacements | null | This works the same way as the languages (Lang::get()) function.


## Usage
This system allows you to set 4 types of messages: status (green), warning (yellow), error (red) and info (blue). Each has its own function.

Set a single status message
```php
Messages::status('Message here')
```

Set a batch of error messages (options can't be used with batch setting)
```php
Messages::error(array(
	'Message 1',
	'Message 2'
))
```

Get all info messages
```php
Messages::info()
```

Set a warning message on the current page, using translations and replacements:
```php
Messages::warning('user.usernametaken',array(
	'temp' => true,
	'priority' => -50,
	'replacements' => array(
		'name' => 'johndoe'
	)
))
```