<?php namespace DeVosBurchart\Acars;

use Session;
use Config;

class Acars
{
	/**
	 * Holds the messages
	 * @var array
	 */
	protected static $messages = array();
	protected static $show = array();
	protected static $statuses = false;
	protected static $init = false;

	/**
	 * Adds messages to the data array
	 *
	 * This is the base function that sets all the messages, it is not directly accessible
	 * 
	 * @param string	$messages_type		type of message (status/error/warning)
	 * @param string	$message			message contents 
	 * @param int		$priority			order message will be shown in
	 */
	
	protected static function set_message($message_type, $message, $options = array()) {
		if(!self::$init) {
			self::$show = (array) Session::get('messages');
			self::$init = true;
		}
		if (in_array($message_type, array('success', 'error', 'warning', 'info')) && $message) {
		
			$priority = array_key_exists('priority', $options) ? $options['priority'] : 0;
			$replacements = array_key_exists('replacements', $options) ? $options['replacements'] : array();
			$class = array_key_exists('class',$options) ? $options['class'] : array();
			
			$message = array('type' => $message_type, 'content' => $message, 'priority' => $priority, 'replacements' => $replacements, 'class' => $class);
			if(array_key_exists('temp', $options) && $options['temp'] === true) self::$show[] = $message;
		} else {
			return false;
		}
		if(!array_key_exists('temp', $options) || $options['temp'] === false):
			self::$messages[] = $message;
			Session::flash('messages',self::$messages);
		endif;
		return $message;
	}
	
	/**
	 * Status/Error/Warning messages
	 *
	 * This function will set or get any message
	 *
	 * The first parameter is the message: if set to false, it will fetch the
	 * messages; if parameter is an array, it will call the batch function to set
	 * each array entry as a message; all other scenarios will be regular messages.
	 *
	 * The priority parameter is only used when setting single messages.
	 * 
	 * @param mixed		$message			message contents 
	 * @param int		$priority			order message will be shown in
	 */
	
	public static function status($message = false, $options = array()) {
		return self::success($message, $options);
	}

	public static function success($message = false, $options = array()) {
		if($message === false)
			return self::get_messages('success');
		elseif(is_array($message)) 
			return self::batch('success',$message);
		else
			return self::set_message('success',$message,$options);
	}

	public static function error($message = false, $options = array()) {
		if($message === false)
			return self::get_messages('error');
		elseif(is_array($message)) 
			return self::batch('error',$message);
		else
			return self::set_message('error',$message,$options);
	}
	
	public static function warning($message = false, $options = array()) {
		if($message === false)
			return self::get_messages('warning');
		elseif(is_array($message)) 
			return self::batch('warning',$message);
		else
			return self::set_message('warning',$message,$options);
	}
	
	public static function info($message = false, $options = array()) {
		if($message === false)
			return self::get_messages('info');
		elseif(is_array($message)) 
			return self::batch('info',$message);
		else
			return self::set_message('info',$message,$options);
	}
	
	/**
	 * Batch set messages
	 *
	 * This function will set any message from an array
	 *
	 * The first parameter is the message type: this would be status, error or warning.
	 * The second parameter is the array of messages.
	 * 
	 * @param string	$message_type		message type, status/error/warning
	 * @param array		$messages			array with message contents
	 */
	
	protected static function batch($message_type,$messages) {
		foreach($messages as $message) {
			self::set_message($message_type,$message);
		}
	}

	/**
	 * Import messages
	 *
	 * This function will set messages through returned data
	 *
	 * The only parameter is the array of messages.
	 * 
	 * @param mixed		$messages			array with message contents
	 * @return void
	 */

	public static function import($messages) {
		if(!self::$init) {
			self::$show = (array) Session::get('messages');
			self::$init = true;
		}

		if(!is_array($messages)) $messages = array($messages);

		foreach($messages as $message) {
			self::$messages[] = $message;
			Session::flash('messages',self::$messages);
		}
	}

	/**
	 * Gets messages for the specified container.
	 *
	 * Grabs an array containing the message and attributes for
	 * a container 
	 *
	 * <code>
	 * $messages = Message::get();
	 * </code>
	 * 
	 * @return array
	 */
	
	protected static function get_messages($message_type = array()) {
		if(!self::$init):
			self::$show = (array) Session::get('messages');
			self::$init = true;
		endif;
	
		if(!is_array($message_type)) $message_type = array($message_type);
		$messages = self::$show;
				
		if(count($messages) == 0) return false;
		
		usort($messages, function($a, $b) {
			return $a['priority'] - $b['priority'];
		});
		
		foreach ($messages as $message) {
			if(in_array($message['type'],$message_type)) {
				$status = '<' . Config::get('acars::tag') . ' class="' . Config::get('acars::classes.global') . ' ' . Config::get('acars::classes.' . $message['type']) . ' ' . implode(' ',$message['class']) . '">';
				$status .= Config::get('acars::html.prepend');
				$status .= \Lang::get($message['content'], $message['replacements']);
				$status .= Config::get('acars::html.append');
				$status .= '</' . Config::get('acars::tag') . '>';
				self::$statuses .= $status;
			}
		}
		
		return self::$statuses;
	}
	
	public static function has() {
		if(!self::$init):
			self::$show = (array) Session::get('messages');
			self::$init = true;
		endif;
	
		if(count(self::$show) == 0) return false;
		return true;
	}
	
	public static function get() {
		return self::get_messages(array('error','success','warning','info'));
	}

}