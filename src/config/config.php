<?php
return array(
	
	'tag' => 'div',
	'classes' => [
		'global' => 'message',
		'success' => 'status',
		'error' => 'error',
		'warning' => 'warning',
		'info' => 'info',
	],
	'html' => [
		'prepend' => '',
		'append' => '',
	],
	
);